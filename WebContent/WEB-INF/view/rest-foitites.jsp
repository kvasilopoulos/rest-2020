

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>List Foitites</title>
<!-- reference our style sheet -->

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
<style>
table, th, td {
  border: 1px solid black;
}
</style>
<sec:authentication property="name" var="User"/>
<script>
$(document).ready(function(){
	 $.ajax({
		 url:("http://localhost:8080/Springmvc1/api/company/all")
	 }).then(function(result) {
        var insert = '';
        for(var i = 0; i < result.length ; i++) {
        	if(result[i].firstoption == '${User}' || result[i].secondoption == '${User}'){
            insert += '<tr><td>' + result[i].id + '</td><td>' + result[i].firstName + '</td><td>' + result[i].lastName + '</td><td>'+ result[i].email + '</td><td>' + result[i].semester +'</td><td>' + result[i].non_passed_subjects +'</td>';    
        }
        }
        $('#foititis tr:last').after(insert);
    });
});
</script>
</head>
	<div id="wrapper">
		<div id="header">
			<h2>Student's List</h2>
		</div>
	</div>
<div>
					<table id="foititis">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>email</th>
                                <th>Semester</th>
                                <th>Non passed subjects</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr></tr>
                        </tbody>
					</table>
				</div>
</body>
</html>
