package gr.hua.dit.springmvc1.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "foititis")
public class Foititis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "email")
	private String email;

	@Column(name = "semester")
	private int semester;
	
	@Column(name = "non_passed_subjects")
	private int non_passed_subjects;

	@Column(name = "qualifiedsemester")
	private boolean qualifiedsemester;
	
	@Column(name = "qualifiednon_passed_subjects")
	private boolean qualifiednon_passed_subjects;
	
	@Column(name = "qualifiedfinal")
	private boolean qualifiedfinal;
	
	@Column(name = "firstoption")
	private String firstoption;
	
	@Column(name = "secondoption")
	private String secondoption;

	public Foititis() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}
	
	public int getNon_passed_subjects() {
		return non_passed_subjects;
	}

	public void setNon_passed_subjects(int non_passed_subjects) {
		this.non_passed_subjects = non_passed_subjects;
	}

	public String getFirstoption() {
		return firstoption;
	}

	public void setFirstoption(String firstoption) {
		this.firstoption = firstoption;
	}

	public String getSecondoption() {
		return secondoption;
	}

	public void setSecondoption(String secondoption) {
		this.secondoption = secondoption;
	}
	
	public boolean getQualifiedsemester() {
		return qualifiedsemester;
	}

	public void setQualifiedsemester(boolean qualifiedsemester) {
		this.qualifiedsemester = qualifiedsemester;
	}
	
	public boolean getQualifiednon_passed_subjects() {
		return qualifiednon_passed_subjects;
	}

	public void setQualifiednon_passed_subjects(boolean qualifiednon_passed_subjects) {
		this.qualifiednon_passed_subjects = qualifiednon_passed_subjects;
	}
	
	public boolean getQualifiedfinal() {
		return qualifiedfinal;
	}

	public void setQualifiedfinal(boolean qualifiedfinal) {
		this.qualifiedfinal = qualifiedfinal;
	}
	
	@Override
	public String toString() {
		return "Foititis [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email +  ", semester=" + semester +  ", non_passed_subjects=" + non_passed_subjects +", firstoption=" + firstoption +", secondoption=" + secondoption + ", qualifiedsemester=" + qualifiedsemester + ", qualifiednon_passed_subjects=" + qualifiednon_passed_subjects + ", qualifiedfinal=" + qualifiedfinal + "]";
	}
	
}