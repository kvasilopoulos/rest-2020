package gr.hua.dit.springmvc1.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "job")
public class Job {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "company")
	private String company;

	@Column(name = "subject")
	private String subject;
	
	@Column(name = "seats")
	private int seats;

	public Job() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public int getSeats() {
		return seats;
	}

	public void setseats(int seats) {
		this.seats = seats;
	}


	@Override
	public String toString() {
		return "Job [id=" + id + ", company=" + company + ", subject=" + subject +  ", seats=" + seats + "]";
	}
	
}