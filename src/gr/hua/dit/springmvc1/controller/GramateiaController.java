package gr.hua.dit.springmvc1.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import gr.hua.dit.springmvc1.dao.GramateiaDAO;
import gr.hua.dit.springmvc1.entity.Foititis;
import gr.hua.dit.springmvc1.entity.Job;

@Controller
@RequestMapping("/gramateia")
public class GramateiaController {

	// inject the customer dao
	@Autowired
	private GramateiaDAO gramateiaDAO;

	
	@GetMapping("/list")
	public String listJob(Model model) {
		// get foitites from dao
		List<Job> jobs = gramateiaDAO.getJobs();

		// add the foitites to the model
		model.addAttribute("job", jobs);

		return "list-jobs";
	}
	
	@GetMapping("/addJob")
	public String showaddJobs(Model model) {
		// create model attribute to get form data
		Job job = new Job();
		model.addAttribute("job", job);
		
		// add page title
		model.addAttribute("pageTitle", "Add a Job");
		return "job-form";
	}
	
	@GetMapping("/list1")
	public String listFoitites(Model model) {
		// get foitites from dao
		List<Foititis> foitites = gramateiaDAO.getFoitites();

		// add the foitites to the model
		model.addAttribute("foititis", foitites);

		return "list-foitites";
	}
	
	@GetMapping("/allpage")
	public String list2Foitites(Model model) {
		// get foitites from dao
		List<Foititis> foitites = gramateiaDAO.getFoitites();

		// add the foitites to the model
		model.addAttribute("foititis", foitites);

		return "rest-foitites";
	}
	
	@GetMapping("/addJob2")
	public String showAddForm2(Model model) {
		// create model attribute to get form data
		Job job = new Job();
		model.addAttribute("job", job);
		
		// add page title
		model.addAttribute("pageTitle", "Add a job");
		return "rest-job";
	}
	
	@PostMapping("/saveJob2")
	public String saveJob2(@ModelAttribute("job") Job job) {
		// save the student using the service
		gramateiaDAO.saveJob(job);
		
		return "redirect:/";
	}
	
	@GetMapping("/{id}")
	public String getFoititis(Model model, @PathVariable("id") int id) {
		// get the foititis
		Foititis foititis = gramateiaDAO.getFoititis(id);
		
		model.addAttribute("foititis", foititis);
		
		return "foititis-form";
	}
	
	@GetMapping("/addFoititis")
	public String showAddForm(Model model) {
		// create model attribute to get form data
		Foititis foititis = new Foititis();
		model.addAttribute("foititis", foititis);
		
		// add page title
		model.addAttribute("pageTitle", "Add a Foititi");
		return "foititis-form";
	}
	
	@PostMapping("/saveFoititis")
	public String saveStudent(@ModelAttribute("student") Foititis foititis) {
		// save the student using the service
		gramateiaDAO.saveFoititis(foititis);
		
		return "redirect:/";
	}
	
	@PostMapping("/saveJob")
	public String saveJob(@ModelAttribute("job") Job job) {
		// save the student using the service
		gramateiaDAO.saveJob(job);
		
		return "redirect:/";
	}

}
