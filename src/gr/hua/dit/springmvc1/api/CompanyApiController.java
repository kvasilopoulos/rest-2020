package gr.hua.dit.springmvc1.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import gr.hua.dit.springmvc1.dao.GramateiaDAO;
import gr.hua.dit.springmvc1.entity.Foititis;
import gr.hua.dit.springmvc1.entity.Job;

@CrossOrigin
@RestController
@RequestMapping("/api/company")
public class CompanyApiController {
	
	@Autowired
	private GramateiaDAO gramateiaDAO;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = { "application/json", "application/xml" })
	public List<Foititis> getFoitites() {
		List<Foititis> foitites = gramateiaDAO.getFoitites();
		return foitites;

	}
	
	
	@RequestMapping(value = "check/{id}", method = RequestMethod.GET, produces = { "application/json", "application/xml" })
	public Foititis getFoititis(@PathVariable("id") int id) {

		Foititis foititis = gramateiaDAO.getFoititis(id);
		return foititis;
		
}
	
	@RequestMapping(value = "/addJob/{company}/{subject}/{seats}", method = RequestMethod.GET, produces = { "application/json", "application/xml" })
	public Job addJob(@PathVariable("company") String company,@PathVariable("subject") String subject,@PathVariable("seats") int seats) {
		Job job = new Job();
		job.setCompany(company);
		job.setSubject(subject);
		job.setseats(seats);
		gramateiaDAO.saveJob(job);
		return job;
	}
}
