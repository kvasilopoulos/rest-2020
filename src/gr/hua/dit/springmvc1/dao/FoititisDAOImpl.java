package gr.hua.dit.springmvc1.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import gr.hua.dit.springmvc1.entity.Foititis;
import gr.hua.dit.springmvc1.entity.Job;

@Repository
public class FoititisDAOImpl implements FoititisDAO {

	// inject the session factory
	@Autowired
	private SessionFactory sessionFactory;
	

	
	@Override
	@Transactional
	public List<Job> getJobs() {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// create a query
		Query<Job> query = currentSession.createQuery("from Job", Job.class);

		// execute the query and get the results list
		List<Job> jobs = query.getResultList();

		// return the results
		return jobs;
	}


	
	@Override
	@Transactional
	public void saveChoice(int id,String firstoption,String secondoption) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		Foititis foititis = currentSession.get(Foititis.class, id);
		// find the Customer
		
			String q = null;
			q = "update Foititis set firstoption = ?1 where id = ?2";
			currentSession.createQuery(q).setParameter(1, firstoption).setParameter(2,id).executeUpdate();
		 
		
			String x = null;
			x = "update Foititis set secondoption = ?1 where id = ?2";
			currentSession.createQuery(x).setParameter(1, secondoption).setParameter(2,id).executeUpdate();
		
					// update the customer
					currentSession.update(foititis);
					// save the student
					currentSession.save(foititis);

	}
	
	@Override
	@Transactional
	public String checknon_passed_subjects(int id) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		String exist;
		// find the Customer
		Foititis Foititis = currentSession.get(Foititis.class, id);
		if(Foititis.getNon_passed_subjects() <= 3) {
			String q = null;
			q = "update Foititis set qualifiednon_passed_subjects = 1 where id = ?1";
			currentSession.createQuery(q).setParameter(1, id).executeUpdate();
			exist = "You are allowed !";
		}else {
			exist = "You cant participate !";
		}
		return exist;

	}
	
	@Override
	@Transactional
	public String checkfinal(int id) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		String exist;
		// find the Customer
		Foititis Foititis = currentSession.get(Foititis.class, id);
		boolean check = Foititis.getQualifiednon_passed_subjects();
		boolean check2 = Foititis.getQualifiedsemester();
		if(check == true && check2 == true) {
			String q = null;
			q = "update Foititis set qualifiedfinal = 1 where id = ?1";
			currentSession.createQuery(q).setParameter(1, id).executeUpdate();
			exist = "You can now applicate for job !";
		}else {
			exist = "You arent allowed to apply for a job !";
		}
		return exist;

	}
	
	@Override
	@Transactional
	public String checksemester(int id) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		String exist;
		// find the Customer
		Foititis Foititis = currentSession.get(Foititis.class, id);
		if(Foititis.getSemester() >= 5 && Foititis.getSemester() <= 8) {
			String q = null;
			q = "update Foititis set qualifiedsemester = 1 where id = ?1";
			currentSession.createQuery(q).setParameter(1, id).executeUpdate();
			exist = "You are allowed !";
		}else {
			exist = "You cant participate !";
		}
		return exist;

	}

}
