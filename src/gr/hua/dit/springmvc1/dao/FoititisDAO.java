package gr.hua.dit.springmvc1.dao;

import java.util.List;

import gr.hua.dit.springmvc1.entity.Foititis;
import gr.hua.dit.springmvc1.entity.Job;

public interface FoititisDAO {
	
	public void saveChoice(int foititis,String firstoption,String secondoption);
	
	public String checknon_passed_subjects(int id);
	
	public List<Job> getJobs();
	
	public String checksemester(int id);
	
	public String checkfinal(int id);
}
